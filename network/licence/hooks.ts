import { useQuery } from "react-query";
import { network } from "./network";

export const hooks = {
  useGetAssurance() {
    const response = useQuery(["licences"], network.getLicence);

    return response;
  },
};
