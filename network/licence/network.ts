import api from "../../lib/axios";

export const network = {
  async getLicence() {
    const { data } = await api.dev("/get_licence");

    return data;
  },
};
